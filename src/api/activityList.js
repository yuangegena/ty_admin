import request from '@/utils/request'

export function getActivityList(params) {
  return request({
    url: '/index/activityList',
    method: 'get',
    params
  })
}
