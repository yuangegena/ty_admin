

const data = {
  list:[
    { months: ['05', '11'],days: ['15'],things: '看电影' },
    { months: ['05', '11'], days: ['02'],things: '去公园野炊' },
    { months: ['05'], days: ['02'],things: '看星星' },
    { months: ['05'], days: ['03'],things: '看月亮' }
  ]
}

export default [
  {
    url: '/vue-admin-template/index/activityList',
    type: 'get',
    response: config => {
      const list = data.list
      return {
        code: 20000,
        data: {
          total: list.length,
          list: list
        }
      }
    }
  }
]
